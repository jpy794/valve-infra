#!/bin/bash

set -ex

. .gitlab-ci/build_functions.sh

build() {
	buildcntr=$(buildah from -v `pwd`:/app/valve-infra --dns=none --isolation=chroot $BASE_IMAGE)
	buildmnt=$(buildah mount $buildcntr)

	cat <<EOF >$buildmnt/etc/resolv.conf
nameserver 1.1.1.1
nameserver 8.8.8.8
nameserver 4.4.4.4
EOF
	$buildah_run $buildcntr pacman -Sy --noconfirm archlinux-keyring
	$buildah_run $buildcntr pacman -Suy --noconfirm
	$buildah_run $buildcntr pacman -S --noconfirm git ansible-core

	# Run ansible playbook, but *only* to install SW. Configuration is done in
	# valve-infra-container.sh
	buildah config --workingdir /app/valve-infra/ansible $buildcntr
	# The Gitlab runner cache deliberately chmod 777's all
	# directories. This upsets ansible and there's nothing we can
	# really do about it in our repo. See
	# https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4187
	$buildah_run $buildcntr chmod -R o-w /app/valve-infra/ansible
	$buildah_run $buildcntr ansible-galaxy collection install -r ./requirements.yml
	# 'install' tag should *only* install SW
	$buildah_run $buildcntr ansible-playbook $ANSIBLE_EXTRA_ARGS ./gateway.yml -l localhost --tags install

	$buildah_run $buildcntr sh -c 'find /usr /etc /root -name __pycache__ -type d | xargs rm -rf'

	# gitlab-runner bundled images aren't used and just take up space
	$buildah_run $buildcntr sh -c 'rm -rf /usr/lib/gitlab-runner/helper-images/'

	$buildah_run $buildcntr sh -c 'env LC_ALL=C pacman -Qi' | awk '/^Name/{name=$3} /^Installed Size/{print $4$5, name}' | sort -h

	# pacman's clean command defaults to 'n' for cache, which makes --noconfirm do
	# exactly what we don't want (nothing)
	$buildah_run $buildcntr sh -c "yes 'y' | pacman -Scc"
}

build_and_push_container
