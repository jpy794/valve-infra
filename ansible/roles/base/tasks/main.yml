---
# Disabled since there is currently no need for this, and it causes issues with
# the way podman currently runs the infra container. Not removed because this
# is needed if/when we try to resurrect DNS-based filtering.
# - name: Ensure custom resolv.conf
#   become: true
#   ansible.builtin.copy:
#     src: resolv.conf
#     dest: /etc/resolv.conf
#     mode: '0744'

- name: Installing base packages
  tags: install
  ansible.builtin.include_role:
    name: pacman_install
  vars:
    pacman_install_pkgs:
      - bash
      - systemd
      - util-linux  # For fstrim

      # HACK: install all the binfmt conf files so that containers
      # can build cross-platform
      - qemu-user-static-binfmt

      # needed for some test jobs:
      - ansible-lint
      - iptables
      - make
      - python-pip
      - python-setuptools
      - qemu-img
      - qemu-system-x86
      - shellcheck
      - slirp4netns
      - yamllint
      - yq

      # for usability
      - bash-completion
      - git
      - iftop
      - nano
      - ncdu
      - openbsd-netcat
      - rsync
      - socat
      - speedtest-cli
      - tcpdump
      - vim
      - wget

- name: Enable timesyncd
  become: true
  ansible.builtin.systemd:
    name: systemd-timesyncd
    enabled: true

- name: disable the first-boot service
  become: true
  ansible.builtin.systemd:
    name: systemd-firstboot.service
    masked: true

- name: Installing sysctl config
  become: true
  ansible.builtin.copy:
    src: "{{ item }}"
    dest: "/etc/sysctl.d/{{ item }}"
    mode: '0644'
  loop:
    - 10-printk.conf
    - 10-disable-ipv6.conf

- name: add a README in /root/
  become: true
  ansible.builtin.copy:
    src: "README.md"
    dest: "/root/README.md"
    mode: '0644'

- name: Creating symlink for /mnt in /root
  become: true
  ansible.builtin.file:
    src: /mnt
    dest: /root/mnt
    state: link
    owner: root
    group: root
    force: true

- name: Creating /etc/systemd/system/fstrim.service.d
  become: true
  ansible.builtin.file:
    path: /etc/systemd/system/fstrim.service.d
    state: directory
    owner: root
    mode: '0755'

- name: Installing fstrim.service override.conf
  become: true
  ansible.builtin.copy:
    src: fstrim-override.conf
    dest: /etc/systemd/system/fstrim.service.d/override.conf
    owner: root
    group: root
    mode: '0644'

- name: Creating /etc/systemd/system/fstrim.timer.d
  become: true
  ansible.builtin.file:
    path: /etc/systemd/system/fstrim.timer.d
    state: directory
    owner: root
    mode: '0755'

- name: Installing fstrim.timer override.conf
  become: true
  ansible.builtin.copy:
    src: fstrim-override.conf
    dest: /etc/systemd/system/fstrim.timer.d/override.conf
    owner: root
    group: root
    mode: '0644'

- name: Enable fstrim
  become: true
  ansible.builtin.systemd:
    name: fstrim.timer
    enabled: true

- name: Create user-cfg group
  become: true
  ansible.builtin.group:
    name: user-cfg

- name: Installing the user-config-setup service
  become: true
  ansible.builtin.template:
    src: user-config-setup.service.j2
    dest: /etc/systemd/system/user-config-setup.service
    owner: root
    group: root
    mode: '0644'
